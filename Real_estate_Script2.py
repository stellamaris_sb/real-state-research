import requests 
import pandas as pd
from bs4 import BeautifulSoup 
import csv 
import re
  
#Get URL

URL = "https://www.globalpropertyguide.com/most-expensive-cities"


#Access URL
r = requests.get(URL) 


#Create a soup object of the content of the url we passed in
  
soup = BeautifulSoup(r.content, 'html5lib') 

#Start using Beautifulsoup tools

#Filtering just the table we want to get 

table = soup.find(id='simpletable')

#Open a CVS file 

file = open('real_estate_prices.csv','w')
priceswriter = csv.writer(file)


#Extracting the values of the table and saving them in a .cvs file


#for table in soup.find_all('li'):
	   #rows=table.find_all('a')
           #print(rows.get())

    
for row in table.find_all('td'):
            rowt=row.get('data-sort-value')
            priceswriter.writerow([rowt])
           
file.close()






